# Sales and Solutions Engineer Technical Assessment : SE_Raymond_Chenon

Account : *SE_Raymond_Chenon*

About me, I have experience in building custom deep links without SDK.
Overall, I enjoyed using the Branch SDK for :
- the powerful URL shortener. You can retrieve a lot of data without passing numerous query parameters.
- the analytics on the user clicks.

## Link settings

In my first account "SE_Raymond_Chenon",  https://flsb.test-app.link is the default link domain , the URL to fallback to install is https://blog.raychenon.com/apk/branchster.apk

After playing with the link domain, I created a second account "SE_Ray_Chenon".
https://raychenon.test-app.link is the default link domain , the fallback URL for installation is https://blog.raychenon.com/apk/branchster-raychenon.apk

Note : the .apk files are release signed . I didn't publish on Google Play to save time.

## Comments
> 1. Track that the user visited the monster edit page (call event "monster_edit") on the MonsterCreatorActivity
> 2. Track that the user visited the monster view page (call event "monster_view") on the MonsterViewerActivity. This time, include state information (specified in `myMonsterObject.monsterMetaData()`)

I implemented the code. But the events are not shown in the Dashboard > Liveview . This [SO post](https://stackoverflow.com/questions/38700983/liveview-shows-no-events) didn't help me, the keys are correct.
I even added the optional dependencies : `customtabs`,`play-services-ads`, `play-services-appindexing`. Then I had to support multidex, these events are still not visible in the dashboard.
This is the type of bugs that could be easily solved in a few minutes by asking help.

> 3. Using an asynchronous method inside `initUI()`, get a shortURL with params, using the static keys provided in `MonsterPreferences` and the values from `myMonsterObject.prepareBranchDict()`. Put that URL into the `monsterUrl` TextView, and be sure to move the `progressBar.setVisibility(View.GONE);` inside the completionhandler
> 4. Inside `shareMyMonster`, get a short URL with params. Get the params from `myMonsterObject.prepareBranchDict()`, set the channel to be "sms", and make sure that you use an asynchronous method.
    1. Only attempt to send the SMS if there is no error in getting the Branch link.

Ok, this is the real test. The Branch link works fine. All the Monster params are contained in referringParams as JSON format in `SplashActivity`.

For the exact same content (monster), the short URL given by `generateShortUrl` can be different .
When an user comes from a referral link, in order to show the same link in `MonsterViewerActivity` as in the SMS, email ... I passed this referring link in an extra field in `MonsterObject` from SplashActivity to MonsterViewerActivity.
