# Sales and Solutions Engineer Technical Assessment
## OVERVIEW
Welcome to Branch's candidate challenge for Sales or Solutions Engineering. We want to get you familiar with the Branch service, specifically on Android. Branch has a lot of moving pieces packed into our SDK, and as a Sales or Solutions Engineer, you'll be touching all aspects.  This exercise should take about 2-4 hours if you’re familiar with mobile development.

## REFERENCES & GUIDES
We encourage you to read through as much documentation as possible. At minimum, you should navigate through the following documentation:

- [Branch Android SDK](https://github.com/BranchMetrics/android-branch-deep-linking)
- [Basic SDK Integration Guide](https://docs.branch.io/pages/apps/android/)
- [Docs Portal](https://docs.branch.io/)
- [Testing and Debugging](https://support.branch.io/support/solutions/articles/6000194357-branch-sdk-test-guide)
- [LiveView Dashboard Page](https://dashboard.branch.io/liveview/links) for testing (after you sign up on the Dashboard)


## BASIC SETUP
1. Create a [fork](https://help.github.com/articles/fork-a-repo/) of this repository.
1. Download Android Studio if you don't have it.
1. Figure out how you'd like to import our SDK into the project.
1. Setup an account on our dashboard: http://dashboard.branch.io
1. Register an application, name it **"SE_YourFirstName_YourLastName"**, and copy your Branch Key. 

## TASKS
If you're able to open the project in Android Studio, you are ready to go!

### Integrate the Branch SDK
1. Follow the SDK integration steps outlined [here](https://docs.branch.io/pages/apps/android/).

### Set up custom event tracking, link creation, and content sharing
1. Track that the user visited the monster edit page (call event "monster_edit") on the MonsterCreatorActivity
1. Track that the user visited the monster view page (call event "monster_view") on the MonsterViewerActivity. This time, include state information (specified in `myMonsterObject.monsterMetaData()`)
1. Using an asynchronous method inside `initUI()`, get a shortURL with params, using the static keys provided in `MonsterPreferences` and the values from `myMonsterObject.prepareBranchDict()`. Put that URL into the `monsterUrl` TextView, and be sure to move the `progressBar.setVisibility(View.GONE);` inside the completionhandler
1. Inside `shareMyMonster`, get a short URL with params. Get the params from `myMonsterObject.prepareBranchDict()`, set the channel to be "sms", and make sure that you use an asynchronous method.
    1. Only attempt to send the SMS if there is no error in getting the Branch link.

## FINAL STEPS
### What the end result should be
The end result should be an Android app that includes the Branch SDK, creates a Branch link for sharing, and deep links appropriately for both existing users (app is already installed) as well as new users (app downloaded after clicking a link). The link you generate for a monster should open the app to that monster.

We're looking for good understanding of code and SDKs. If not everything works, don't worry. We want to see your understanding of Branch's product.

### Submitting your work
1. Commit all of your changes to your fork
1. Create a [pull request](https://help.github.com/articles/creating-a-pull-request-from-a-fork/) from your fork back to this repo
